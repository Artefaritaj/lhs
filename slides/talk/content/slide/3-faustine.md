+++
contentType = "md"
weight = 3
+++
---
class: center, middle, inria

# Faustine platform

## For fault injection attacks

.inria-back[
<img src="./img/Inria-0279-137.jpg" height="90%" width="90%" />
]
<small>© Inria / Photo C. Morel</small>

---
class: middle, inria

# What can we do with Faustine

## Devices

- Send EM pulses in the 80MHz-1GHz bandwidth.
- Successful attacks on Smartcard, microcontrollers, ARM SoC (@1.2GHz).

## Applications

- Cryptanalysis.
- PIN code retrieval.
- Any attack requiring editing code at runtime.
- [your application ?]

## Fault effect

Most of the time, the fault allows to delete the targeted instruction (replaced with a nop instead).
But other models are achievable: randomized opcode, fault on loaded data, multiple faults...

---
class: middle, inria

# Current research with Faustine

Focusing on non-crypto applications:

- Control flow highjacking
- Overcoming buffer overflow countermeasures
- **Fault activated backdoor**

Exemple on a microcontroller: single-core, <50MHz, simple architecture.

.center[
<img src="./img/Inria-0279-140.jpg" height="55%" width="55%" />
]
<small>© Inria / Photo C. Morel</small>

---
class: inria

# Fault activated backdoor

```C
void blink_wait()
{
  unsigned int wait_for = 3758874636;
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
```

=>

```ASM
08000598 <blink_wait>:
push	{r7, lr}
sub	sp, #8
add	r7, sp, #0
ldr	r3, [pc, #44]	; (80005cc <blink_wait+0x34>)
...
adds	r7, #8
mov	sp, r7
pop	{r7, pc}
.word	0xe00be00c ; @80005cc, 0xe00be00c = 3758874636
```

---
class: inria

# How to activate the backdoor?

```C
void blink_wait()
{
  unsigned int wait_for = 3758874636;//go to backdoor
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
```

---
class: inria

# How to activate the backdoor?

```C
void blink_wait()
{
  unsigned int wait_for = 3758874636;//go to backdoor
  unsigned int counter;
  for(counter = 0; counter < wait_for; counter += 8000000);
}
```

=>

```ASM
08000598 <blink_wait>:
push	{r7, lr}
sub	sp, #8
add	r7, sp, #0
ldr	r3, [pc, #44]	; (80005cc <blink_wait+0x34>)
...
adds	r7, #8
mov	sp, r7
nop ; pop	{r7, pc}
b backdoor_payload ;.word	(0xe00b)e00c
b backdoor_payload ;.word	0xe00b(e00c)
```

---
class: middle, inria

# Other work with Faustine

Modern System-on-Chip are also targeted: multi-core, >1GHz, complex architecture.

We prove (work in progress) that we can attack subsystems:
- caches,
- MMU,
- memory coherence unit.

Opening up a realm of new possibilities.

---

class: middle, inria

# Faustine platform

Spectre, Meltdown attacks are just the beginning. 

Modern systems are **MORE** vulnerable at the hardware level (but exploiting this vulnerability requires time, knowledge and means).

How should we protect ourselves ?


.center[
<img src="./img/iceberg.jpg" height="50%" width="70%" />
]

**If you have ideas and want to do experiments in the LHS, come talk with us !**

---
class: middle, inria

# We also have targets

A great amount of work has been put into target developments:
- applications for microcontroller, smartcards,
- bare metal (without OS) applications on ARMv8,
- hardware targets in developement with RISC-V cores.

.center[
## INRIA will join the RISC-V foundation in 2019
]

---
class: center, middle, inria

# Thank you for your attention

Any question ?


<img src="./img/Inria-0279-137.jpg" height="90%" width="90%" />

<small>© Inria / Photo C. Morel</small>
