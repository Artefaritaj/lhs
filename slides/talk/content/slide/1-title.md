+++
contentType = "md"
weight = 1
+++
class: center, middle, inria

.logo[
![INRIA logo](./img/inria-logo.png)
]

# Physical attacks in the LHS

EMA and Faustine platforms


Examples taken from the work of *Sébanjila Kevin Bukasa, Ludovic Claudepierre, Mathieu Escouteloup, Jean-Louis Lanet, Ronan Lashermes and Hélène Le Bouder*.


.pull-right[from the LHS @ INRIA-RBA]


---
class: inria, middle

# To evaluate the security of devices and applications

Exploiting the chip physical environment to attack it.

The attacker must have physical access to the system.
- Smartcard,
- IoT,
- Phones,
- Smart Keys (cars, hotels, ...),
- ...
