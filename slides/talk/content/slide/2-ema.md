+++
contentType = "md"
weight = 2
+++
---
class: center, middle, inria

# Physical attacks Lab
&nbsp;
.inria-back[
<img src="./img/Inria-0279-123.jpg" height="90%" width="90%" />
]
<small>© Inria / Photo C. Morel</small>

---
class: center, middle, inria

# EMA platform

## For observation attacks

.inria-back[
<img src="./img/Inria-0279-143.jpg" height="90%" width="90%" />
]
<small>© Inria / Photo C. Morel</small>

---
class: middle, inria

# What can we do with EMA ?

## Devices

- Measuring EM radiation in the 0-3GHz bandwidth.
- Successful attacks on Smartcard, microcontrollers, ARM SoC (@1.2GHz).

## Applications

- Cryptanalysis
- PIN code retrieval
- Reverse-engineering
- [your application ?]

---
class: middle, inria

# What can we do with EMA ?

.center[
<img src="./img/cor.png" height="60%" width="60%" />
]

## We have training materials (CC BY-NC 4.0)

Measure data and Julia (v1.0) code to find the key of AES encryptions.
Can be found here (EMA on gitlab.inria.fr): [EMA gitlab](https://gitlab.inria.fr/rlasherm/EMA/tree/master/Training/STM32_5k)
